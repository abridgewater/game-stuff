;;;
;;; text-widget.lisp
;;;
;;; A text display area.
;;;

(cl:defpackage :game-stuff/text-widget
  (:use :cl :game-stuff/scratch-widgets :game-stuff/scratch-drawing
	:game-stuff/scratch-input)
  (:export
   "TEXT-WIDGET" "CLEAR-TEXT-OUTPUT"))
(cl:in-package :game-stuff/text-widget)


;;; Storage for lines of text.

(defun make-blank-line ()
  (make-array 15 :adjustable t :fill-pointer 0 :element-type 'character))

(defun make-empty-line-buffer ()
  (let ((lines (make-array 15 :adjustable t :fill-pointer 0)))
    (vector-push (make-blank-line) lines)
    lines))


;;; A widget for displaying a text message (badly incomplete).

(defclass text-widget (widget sb-gray:fundamental-character-output-stream)
  ((x :initform 0 :initarg :x)
   (y :initform 0 :initarg :y)
   (width :initform 0 :initarg :width)
   (height :initform 0 :initarg :height)
   (lines :initform (make-empty-line-buffer) :initarg :lines)))

;; For SBCL, this one method is the minimum required for text output
;; to "work".
(defmethod sb-gray:stream-write-char ((widget text-widget) character)
  (with-slots (lines) widget
    (if (eql character #\Newline)
	(vector-push-extend (make-blank-line) lines)
	(vector-push-extend character (aref lines (1- (length lines)))))))

(defun clear-text-output (widget)
  (setf (slot-value widget 'lines) (make-empty-line-buffer)))

(defmethod render-widget ((widget text-widget))
  ;; Draw a window backdrop.
  ;(gl:color-3ub 0 0 127) ;; There's a color-4b if we want transparency.
  (gl:enable gl:+blend+)
  (gl:blend-func gl:+src-alpha+ gl:+one-minus-src-alpha+)
  (gl:color-4ub 0 0 127 127)

  (with-slots (x y width height) widget
    (draw-box x y width height)

    ;; Specify yellow text.
    (gl:color-3ub 255 255 0)

    (loop
       with lines = (slot-value widget 'lines)
       with line
       for i from 0 below (1- (floor height 16))
       while (< i (length lines))
       do (setf line (aref lines (- (length lines) i 1)))
       unless (zerop (length line))
       do
	 (gl:raster-pos-2i (+ x 16) (- (+ y height) 16 (* i 16)))
	 (render-gl-text line))
  ))

;;; EOF
