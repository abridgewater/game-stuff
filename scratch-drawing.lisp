;;;
;;; scratch-drawing.lisp
;;;
;;; Drawing functions from scratch space.
;;;

(cl:defpackage :game-stuff/scratch-drawing
  (:use :cl)
  (:export
   "RENDER-GL-TEXT"
   "DRAW-BOX"
   "DRAW-MENU-CURSOR"))
(cl:in-package :game-stuff/scratch-drawing)


(defun render-gl-text (string)
  ;; Output a text string (feel free to wince).
  (let* ((octets (make-array (length string)
			     :element-type 'character
			     :initial-contents string)))
    (sb-sys:without-gcing
      ;; KLUDGE: Convert hello-octets from a string to an octet
      ;; vector by doing unspeakable things to its in-memory
      ;; representation.  There are two rules for doing this sort of
      ;; thing.  Rule one: Don't.  Rule two (for experts only):
      ;; Don't unless you know precisely how wrong this can go.
      (let ((obj-sap (sb-sys:int-sap
		      (sb-kernel:get-lisp-obj-address
		       octets))))
	(symbol-macrolet
	    ((header (sb-sys:sap-ref-word obj-sap
					  (- sb-vm:other-pointer-lowtag)))
	     (length (sb-sys:sap-ref-word obj-sap
					  (- (* sb-vm:vector-length-slot
						sb-vm:n-word-bytes)
					     sb-vm:other-pointer-lowtag))))
	  (setf header sb-vm::simple-array-unsigned-byte-8-widetag)
	  (setf length (* length 4)))))
    (gl::with-large-render-request (2 octets)
      (gl::int32 (length string))
      (gl::card32 gl:+unsigned-int+))))

(defun draw-box (x y width height)
  (gl:begin gl:+polygon+)
  (gl:vertex-2i x y)
  (gl:vertex-2i (+ x width) y)
  (gl:vertex-2i (+ x width) (+ y height))
  (gl:vertex-2i x (+ y height))
  (gl:end))

(defun draw-menu-cursor (x y)
  (gl:begin gl:+polygon+)
  (gl:vertex-2i x y)
  (gl:vertex-2i (+ x 6) (+ y 6))
  (gl:vertex-2i x (+ y 12))
  (gl:end))

;;; EOF
