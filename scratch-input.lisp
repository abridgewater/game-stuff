;;;
;;; scratch-input.lisp
;;;
;;; Input handling from scratch space.
;;;

(cl:defpackage :game-stuff/scratch-input
  (:use :cl :game-stuff/clx-interface)
  (:export
   "INPUT-CONTEXT"
   "*INPUT-CONTEXT*"
   "*INPUT-FUNCTION*"

   "UPDATE-INPUT-CONTEXT"
   "HANDLE-INPUT-EVENT"

   "KEY-UP"    "LAST-KEY-UP"
   "KEY-DOWN"  "LAST-KEY-DOWN"
   "KEY-LEFT"  "LAST-KEY-LEFT"
   "KEY-RIGHT" "LAST-KEY-RIGHT"
   "KEY-ESC"   "LAST-KEY-ESC"
   "KEY-ENTER" "LAST-KEY-ENTER"))
(cl:in-package :game-stuff/scratch-input)


(defclass input-context ()
  ((key-up :initform nil)
   (last-key-up :initform nil)
   (key-down :initform nil)
   (last-key-down :initform nil)
   (key-left :initform nil)
   (last-key-left :initform nil)
   (key-right :initform nil)
   (last-key-right :initform nil)
   (key-esc :initform nil)
   (last-key-esc :initform nil)
   (key-enter :initform nil)
   (last-key-enter :initform nil)))

(defvar *input-context*)
(defvar *input-function* nil)

(defun update-input-context ()
  (with-slots (key-up    last-key-up
	       key-down  last-key-down
	       key-left  last-key-left
	       key-right last-key-right
	       key-esc   last-key-esc
	       key-enter last-key-enter)
      *input-context*
    (setf last-key-up    key-up
	  last-key-down  key-down
	  last-key-left  key-left
	  last-key-right key-right
	  last-key-esc   key-esc
	  last-key-enter key-enter)))

(defun handle-input-event (key-code down-p)
  (let ((keysym (xlib:keycode->keysym *display* key-code 0)))
    (declare (integer keysym))
    (case keysym
      (#.+xk-up+    (setf (slot-value *input-context* 'key-up)    down-p))
      (#.+xk-down+  (setf (slot-value *input-context* 'key-down)  down-p))
      (#.+xk-left+  (setf (slot-value *input-context* 'key-left)  down-p))
      (#.+xk-right+ (setf (slot-value *input-context* 'key-right) down-p))
      (#.+xk-esc+   (setf (slot-value *input-context* 'key-esc)   down-p))
      (#.+xk-enter+ (setf (slot-value *input-context* 'key-enter) down-p))
      (t (unless down-p
	   (format t "Unknown keysym ~x~%" keysym))))))

;;; EOF
