;;;
;;; scratch.lisp
;;;
;;; Scratch space for hacking things up.
;;;

(cl:defpackage :game-stuff/scratch
  (:use :cl :game-stuff/clx-interface :game-stuff/space-requirement
	:game-stuff/scratch-input :game-stuff/scratch-widgets
	:game-stuff/scratch-drawing :game-stuff/maze-runner
	:game-stuff/menu-widget :game-stuff/text-widget))
(cl:in-package :game-stuff/scratch)


(defun game-loop (update-function repaint-function)
  (loop
     (funcall update-function)
     (funcall repaint-function)
     (sleep 0.05)
     (update-input-context)
     (xlib:event-case (*display* :discard-p t :timeout 0)
       ;;(:exposure (count) (when (zerop count) (funcall repaint-function)) nil)
       (:exposure () nil)
       (:key-press (code) (handle-input-event code t) nil)
       (:key-release (code) (handle-input-event code nil) nil))))

(defun test-game-loop-1/update ()
  (with-slots (key-up    last-key-up
	       key-down  last-key-down
	       key-left  last-key-left
	       key-right last-key-right
	       key-esc   last-key-esc)
      *input-context*
    (when (and last-key-esc (not key-esc))
      ;; Key-up on ESC, quit.
      (throw '%exit-game nil))

    (when *input-function*
      (funcall *input-function*))
    ))

;; Override default (empty) list of active widgets.
(defparameter *active-widgets*
  (list (make-instance 'control-status-widget)
	;(make-instance 'menu-widget :x 32 :y 32 :width 64 :height 72 :items '("Attack" "Item" "Magic" "Run") :current-item 0)
	;(make-instance 'polygon-widget :x 256 :y 192 :sides 5 :radius 80 :theta 0)
	;(make-instance 'text-widget :x 32 :y 320 :width 448 :height 160)
	(make-instance 'maze-widget)
	))

(defun test-game-loop-1/redisplay ()
  ;; Clear to the background.
  (gl:clear gl:+color-buffer-bit+)

  ;; Render all active widgets (on list, from back to front).
  (labels ((render-widget-list (list)
	     (when list
	       (render-widget-list (cdr list))
	       (render-widget (car list)))))
    (render-widget-list *active-widgets*))

  ;; And display the resulting mess.
  (glx:swap-buffers))

(defun test-game-loop-1 ()
  (with-x11-display (:space-requirement
		     (make-space-requirement :width 512 :height 512
					     :min-width 512 :min-height 512
					     :max-width 512 :max-height 512)
		     :window-title "Test Game Loop 1")
    ;; A window comes with exposure events enabled, so watch for one
    ;; to see if we're actually visible.
    (xlib:event-case (*display* :discard-p t)
      (:exposure (count) (zerop count)))

    ;; Load us a font and prep it for use.
    (let ((font (xlib:open-font *display* "7x14"))
	  (lists (gl:gen-lists 256)))
      (glx:use-x-font font 0 256 lists)
      (gl:list-base lists))

    ;; Clear to black.
    (gl:clear-color 0.0s0 0.0s0 0.0s0 0.0s0)

    ;; Set up a basic 1:1 pixel 2d projection matrix.
    (gl:matrix-mode gl:+projection+)
    (gl:load-identity)
    (gl:ortho 0.0d0 512.0d0 512.0d0 0.0d0 -1.0d0 1.0d0)

    ;; Set up a fresh input context.
    (setf *input-context* (make-instance 'input-context))

    ;; Set up the maze runner.
    (reset-maze)
    (setf *input-function* 'maze-widget-input)

    ;; We've had an exposure.  We might not get another one too soon.
    (test-game-loop-1/redisplay)

    ;; Enable keyboard events.
    (setf (xlib:window-event-mask *window*)
	  (logior (xlib:window-event-mask *window*)
		  (xlib:make-event-mask :key-press :key-release)))

    (catch '%exit-game
      (game-loop 'test-game-loop-1/update
		 'test-game-loop-1/redisplay))))

;;; EOF
