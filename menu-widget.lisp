;;;
;;; menu-widget.lisp
;;;
;;; An in-game menu.
;;;

(cl:defpackage :game-stuff/menu-widget
  (:use :cl :game-stuff/scratch-widgets :game-stuff/scratch-drawing
	:game-stuff/scratch-input)
  (:export
   "MENU-WIDGET"))
(cl:in-package :game-stuff/menu-widget)


(defclass menu-widget (widget)
  ((x :initform 0 :initarg :x)
   (y :initform 0 :initarg :y)
   (width :initform 0 :initarg :width)
   (height :initform 0 :initarg :height)
   (items :initform '("Yes" "No") :initarg :items)
   (current-item :initform 0 :initarg :current-item)))

(defmethod render-widget ((widget menu-widget))
  (with-slots (x y width height items current-item) widget

    ;; Draw a window backdrop.
    (gl:color-3ub 0 0 127)
    (draw-box x y width height)

    ;; Set yellow text.
    (gl:color-3ub 255 255 0)

    ;; Draw some menu items.
    (loop
       for item in items
       for index from 0
       do
	 (gl:raster-pos-2i (+ x 16) (+ y (* (1+ index) 16)))
	 (render-gl-text item))

    ;; Draw the current-item marker.
    (draw-menu-cursor (+ x 6) (+ y 5 (* current-item 16)))))

(defclass menu-widget-controller ()
  ((widget :initarg :widget)
   (max-item :initarg :max-item)
   (old-input-function :initarg :old-input-function)))

(defmethod controller-input-function ((controller menu-widget-controller))
  (with-slots (widget old-input-function max-item) controller
    (lambda ()
      (with-slots (key-up    last-key-up
		   key-down  last-key-down
		   key-left  last-key-left
		   key-right last-key-right
		   key-esc   last-key-esc
		   key-enter last-key-enter)
	  *input-context*
	(with-slots ((menu-items items) current-item) widget
	  (cond
	    ((and last-key-enter (not key-enter))
	     ;; release transition on enter, we're done.
	     (setf *active-widgets* (remove widget *active-widgets*))
	     (setf *input-function* old-input-function)
	     (format t "Menu item chosen: ~S~%"
		     (elt menu-items current-item)))

	    ((and last-key-esc (not key-esc))
	     ;; release transition on esc, cancel if we can.
	     )

	    ((and key-up (not last-key-up))
	     ;; press transition on up, move to previous entry.
	     (setf current-item (max 0 (1- current-item))))

	    ((and key-down (not last-key-down))
	     ;; press transition on down, move to next entry.
	     (setf current-item (min max-item (1+ current-item))))))))))

(defun test-menu-input ()
  (let* ((menu-items '("Yes" "No"))
	 (max-item (1- (length menu-items)))
	 (widget (make-instance 'menu-widget
				:x 64 :y 64 :width 48 :height 48
				:items menu-items
				:current-item 0))
	 (controller (make-instance 'menu-widget-controller
				    :widget widget
				    :max-item max-item
				    :old-input-function *input-function*)))
    (push widget *active-widgets*)
    (setf *input-function* (controller-input-function controller))))

;;; EOF
