;;;
;;; scratch-widgets.lisp
;;;
;;; Widgets from scratch space.
;;;

(cl:defpackage :game-stuff/scratch-widgets
  (:use :cl :game-stuff/scratch-input :game-stuff/scratch-drawing)
  (:export
   "WIDGET"
   "RENDER-WIDGET"

   "*ACTIVE-WIDGETS*"

   "CONTROL-STATUS-WIDGET"
   "TEXT-MESSAGE-WIDGET"
   "POLYGON-WIDGET"))
(cl:in-package :game-stuff/scratch-widgets)


;;; Generic "visible widget" interface.

(defclass widget () ())

(defgeneric render-widget (widget))

(defmethod render-widget ((widget widget)))

(defvar *active-widgets* nil)


;;; A widget for reporting the status of various bits of input (a
;;; debugging aid, really).

(defclass control-status-widget (widget) ())

(defmethod render-widget ((widget control-status-widget))
  (gl:color-3ub 255 255 0)

  (when (slot-value *input-context* 'key-left)
    (draw-box 16 96 16 16))

  (when (slot-value *input-context* 'key-right)
    (draw-box 64 96 16 16))

  (when (slot-value *input-context* 'key-up)
    (draw-box 40 72 16 16))

  (when (slot-value *input-context* 'key-down)
    (draw-box 40 120 16 16)))


;;; A widget for displaying a regular polygon.

(defclass polygon-widget (widget)
  ((x :initform 0 :initarg :x)
   (y :initform 0 :initarg :y)
   (sides :initform 3 :initarg :sides)
   (radius :initform 5 :initarg :radius)
   (theta :initform 0 :initarg :theta)))

(defmethod render-widget ((widget polygon-widget))
  (gl:color-3ub 255 255 0)
  (with-slots (x y sides radius theta) widget
    (gl:begin gl:+polygon+)
    (let ((alpha (/ (* pi 2) sides)))
      (dotimes (i sides)
	(let* ((angle (+ theta (* i alpha)))
	       ($x_i$ (+ x (* radius (sin angle))))
	       ($y_i$ (- y (* radius (cos angle)))))
	  ;; FIXME: Make GL commands coerce automatically.
	  (gl:vertex-2f (float $x_i$ 0.0s0) (float $y_i$ 0.0s0)))))
    (gl:end)))

;;; EOF
