;;;
;;; maze-runner.lisp
;;;
;;; A simple maze-running widget.
;;;

(cl:defpackage :game-stuff/maze-runner
  (:use :cl :game-stuff/scratch-widgets :game-stuff/scratch-drawing
	:game-stuff/scratch-input)
  (:export
   "MAZE-WIDGET"
   "MAZE-WIDGET-INPUT"
   "RESET-MAZE"))
(cl:in-package :game-stuff/maze-runner)


(defparameter *raw-map-data*
  '("xxxxxxxxxxxxxxxx"
    "x.xxxxxxxxx...xx"
    "x....x.....xx..x"
    "x.xx...xxx.xxx.x"
    "x.xxxxxxxx...x.x"
    "x......xxxxx.x.x"
    "xxxxxx.xxxxx.x.x"
    "xxxxx....xx..x.x"
    "xxxxx.xx.xx.x..x"
    "xx....xxxxx.x.xx"
    "xx.xx...xxx.x..x"
    "xx.xxxx...x.xx.x"
    "xx.xxxxxx...xx.x"
    "xx.xx....xxx...x"
    "xx....xx.....xxx"
    "xxxxxxxxxxxxxxxx")
  "The raw map data, in easily editable form.")

(declaim (type (simple-array t (#x100)) *map-data*))
(defvar *map-data* (make-array #x100)
  "Map data. Each cell is either T for a wall or NIL for empty space.")

(defvar *position* #x11 "Position of player within *map-data*.")
(defvar *facing* :south "Direction player is facing.")
(defvar *frontstep* 0)
(defvar *leftstep* 0)

(defparameter *frontstep-list* '(:north -16 :south 16 :west -1 :east 1)
  "alist from directions to index change within map data to move forward.")
(defparameter *leftstep-list* '(:north -1 :south 1 :west 16 :east -16)
  "alist from directions to index change within map data to move left.")
(defparameter *leftturn-list* '(:north :west :west :south :south :east :east :north)
  "alist from direction to direction for turning left.")
(defparameter *rightturn-list* '(:north :east :east :south :south :west :west :north)
  "alist from direction to direction for turning right.")


(defun init-map-data ()
  "Convert the raw map data in *raw-map-data* to the internal representation in *map-data*."
  (let ((row-number 0))
    (dolist (row-data *raw-map-data*)
      (dotimes (i 16)
	(setf (aref *map-data* (+ i (* 16 row-number)))
	      (and (not (char= (aref row-data i) #\.))
		   (aref row-data i))))
      (incf row-number)))
  (values))

(defun set-facing (direction)
  "Set the player to be facing in DIRECTION. Sets up *frontstep* and *leftstep* for rendering and motion control."
  (setf *facing* direction)
  (setf *frontstep* (getf *frontstep-list* direction))
  (setf *leftstep* (getf *leftstep-list* direction)))

(defun turn-left ()
  "Turn the player 90 degrees to the left."
  (set-facing (getf *leftturn-list* *facing*)))

(defun turn-right ()
  "Turn the player 90 degrees to the right."
  (set-facing (getf *rightturn-list* *facing*)))

(defun move-forward ()
  "Move the player one space forward if there is no wall ahead."
  (if (not (aref *map-data* (+ *position* *frontstep*)))
      (setf *position* (+ *position* *frontstep*))))

(defun draw-left-side (position base size)
  (if (aref *map-data* (+ position *leftstep*))
      (progn
	;; There is a wall to the left of this position, so we draw it.
	(gl:begin gl:+polygon+)
	(gl:vertex-2i base base)
	(gl:vertex-2i (+ base size) (+ base size))
	(gl:vertex-2i (+ base size) (- 512 base size))
	(gl:vertex-2i base (- 512 base))
	(gl:end))
      (progn
	;; There is no wall to the left of this position, so there is one
	;; ahead of it. We draw that one.
	(draw-box base (+ base size) size (- 512 base base size size)))))

(defun draw-right-side (position base size)
  (if (aref *map-data* (- position *leftstep*))
      (progn
	;; There is a wall to the right of this position, so we draw it.
	(gl:begin gl:+polygon+)
	(gl:vertex-2i (- 512 base) base)
	(gl:vertex-2i (- 512 base size) (+ base size))
	(gl:vertex-2i (- 512 base size) (- 512 base size))
	(gl:vertex-2i (- 512 base) (- 512 base))
	(gl:end))
      (progn
	;; There is no wall to the right of this position, so there is one
	;; ahead of it. We draw that one.
	(draw-box (- 512 base size) (+ base size) size (- 512 base base size size)))))

(defun draw-facing-wall (facing-wall base depth)
  (if (eql #\x facing-wall)
      ;; Simple case first, a straight-up wall.
      (draw-box base (1- base)
		(- 512 base base) (+ 2 (- 512 base base)))
      ;; Now the more complicated portals.
      (let* ((top (1- base))
	     (height (+ 2 (- 512 base base)))
	     (width (floor (* (- 256 base) 1/2)))
	     (lintel (floor height 5)))
	(draw-box base top width height)
	(draw-box (+ base width) top (- 512 base base width width) lintel)
	(draw-box (- 512 base width) top width height)

	;; Only draw portal contents if it's right in front.
	(unless (zerop depth)
	  (return-from draw-facing-wall))

	(case facing-wall
	  ;; FIXME: Fill these in, and make them look not-horrible.
	  (#\d ;; door
	   (draw-box (+ base width 1) (+ top lintel 1)
		     (- 512 base base width width 2)
		     (- height lintel 1)))
	  (#\< ;; stairs up
	   )

	  (#\> ;; stairs down
	   )))))

(defun draw-maze ()
  "Draw the maze as seen from the player's current position and facing."
  (let ((base 0)
	(position *position*))
    (dotimes (depth 4)
      ;; size values determined empirically.
      (let ((size (elt '(20 100 80 30) depth)))
	(draw-left-side position base (1- size))
	(draw-right-side position base (1- size))
	
	(incf position *frontstep*)
	(incf base size)
	
	;; Draw the facing wall if there is one.
	(let ((facing-wall (aref *map-data* position)))
	  (when facing-wall
	    (draw-facing-wall facing-wall base depth)
	    (return-from draw-maze))))))
  (values))

(defclass maze-widget (widget) ())

(defmethod render-widget ((widget maze-widget))
  (gl:color-3ub 63 63 63)
  (draw-maze))

(defun maze-widget-input ()
  (with-slots (key-up    last-key-up
	       key-down  last-key-down
	       key-left  last-key-left
	       key-right last-key-right
	       key-esc   last-key-esc
	       key-enter last-key-enter)
      *input-context*
    (cond
      ((and key-up (not last-key-up))
       ;; press transition on up, move forward.
       (move-forward))
      ((and key-left (not last-key-left))
       ;; press transition on left, turn left.
       (turn-left))
      ((and key-right (not last-key-right))
       ;; press transition on right, turn right.
       (turn-right)))))

(defun reset-maze ()
  (init-map-data)
  (set-facing :south))

;;; EOF
